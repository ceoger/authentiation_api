class RegistrationsController < ApplicationController
  def create
    @user = User.new(user_params)
    # user = User.create!(
    #   first_name: params['user']['first_name'],
    #   last_name: params['user']['last_name'],
    #   email: params['user']['email'],
    #   password: params['user']['password'],
    #   password_confirmation: params['user']['password_confirmation']
    # )
    if @user.save
        # Tell the UserMailer to send a welcome email after save
        # UserMailer.with(user: @user).welcome_email.deliver_later
        render json: @user, status: :created
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  #   if @user.save
  #     session[:user_id] = user.id
  #     render json: {
  #       status: :created,
  #       user: user
  #     }
  #   else
  #     render json: {status: 500}
  #   end
  # end
  def user_params
    params.permit(:email, :password, :password_confirmation, :first_name, :last_name)

  end
end
